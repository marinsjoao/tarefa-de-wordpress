<?php
// Template Name: Pagina Quem Somos
?>
<?php get_header(); ?>
      <main id="mainqs">
        <h1 id="titleqs"><?php the_title() ?> </h1>
        <div id="abtqs"><?php the_content( )?> </div>
      </main>
<?php get_footer(); ?>      