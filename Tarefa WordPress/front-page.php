<?php
// Template Name: Pagina Inicial
?>
<?php get_header(); ?>
    <main>
        <div id="adoteLobinho">
            <div>
                <h1 class="fonte"><?php the_field("titulo_inicial") ?></h1>
                <span class="barra"> </span>
                <?php the_field("descr_inicial") ?>
            </div>
        </div>

        <div id="sobre">

            <div>
                <h2><?php the_field("titulo_secundario") ?></h2>
                <p><?php the_field("descr_secundario") ?></p>
            </div>

        </div>

        <div id="valores">
            <div id="escrito-valores">
                <h2>
                <p><?php the_field("titulo_valores") ?></p>
                </h2>
            </div>
            <div id="content-valores">
                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-1">                         
                    </div> 
                    <div class="text-block">
                        <h3><?php the_field("valores_valores_titulos_1") ?></h3>
                        <p><?php the_field("valores_valores_descr_1") ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-2">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field("valores_valores_titulos_2") ?></h3>
                        <p><?php the_field("valores_valores_descr_1") ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-3">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field("valores_valores_titulos_3") ?></h3>
                        <p><?php the_field("valores_valores_descr_1") ?></p>
                    </div>
                </div>

                <div class="block">
                    <div class="img-block">
                        <img class="img-valores-4">
                    </div>
                    <div class="text-block">
                        <h3><?php the_field("valores_valores_titulos_3") ?></h3>
                        <p><?php the_field("valores_valores_descr_1") ?></p>
                    </div>
                </div>

            </div>
        </div>

        <div id="exemploLobos">
            <div id="content-ex">
                <div class="escrito-lobosEx">
                    <h2>Lobos Exemplos</h2>
                </div>
                <div id="lobos-content">
                    <?php
                    $the_query = new WP_Query('posts_per_page=2')
                    ?>
                    <?php $numero = 0 ?>
                    <?php 
                    while ($the_query -> have_posts()) : $the_query -> the_post();
                    ?>

                    <?php if($numero %2 ==0){ ?>
                    
                    <div class="bloco-lobo left">
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img src="<?php the_field('lobo_foto'); ?>" class="img-left" >
                            <?php endif; ?>
                        </div>
                        <div class="info-lobo left-info">
                            <h1><b><?php the_field("lobo_titulo") ?></b></h1>
                            <h4>Idade: <?php the_field("lobo_idade") ?> anos</h4>
                            <p><b><?php the_field("lobo_descr") ?></b></p>
                        </div>
                    </div>
                    <?php } 
                    else{ ?>
                    <div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b><?php the_field("lobo_titulo") ?></b></h1>
                            <h4>Idade: <?php the_field("lobo_idade") ?> anos</h4>
                            <p><b><?php the_field("lobo_descr") ?></b></p>
                        </div>
                        <div class="img-lobo">
                            
                            <?php if( get_field('lobo_foto') ): ?>
                                <img src="<?php the_field('lobo_foto'); ?>" class="img-right" >
                            <?php endif; ?>
                        
                        </div>
                    </div>
                    <?php }
                    $numero++; ?>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>    

                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>
    