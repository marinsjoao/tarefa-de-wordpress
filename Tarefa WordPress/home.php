<?php
// Template Name: Pagina Lista
?>
<?php get_header(); ?>
    
<main id="mainll">
        <div id="topbody">
            <div id="searchadd">
                <div id="search">
                    <img src="<?php echo get_stylesheet_directory_uri()?>/assets/lupinha.png" id="searchimg">
                    <input type="text" id="searchtext">
                </div>
                <a id="btnaddlobo" href="../adicionar-lobinho/adicionar-lobinho.html">+ Lobo</a>
            </div>
            <div id="checkadotados">
                <input type="checkbox" id="checkbox">
                <p id="checktext">Ver lobinhos adotados</p>
            </div>
        </div>
                    
                    
                    
                    
                    <?php
                    $the_query = new WP_Query('posts_per_page')
                    ?>
                    <?php $numero = 0 ?>
                    <?php 
                    while ($the_query -> have_posts()) : $the_query -> the_post();
                    ?>

                    <?php if($numero %2 ==0){ ?>
                    
                    <div class="bloco-lobo left">
                        <div class="img-lobo">
                            <?php if( get_field('lobo_foto') ): ?>
                                <img src="<?php the_field('lobo_foto'); ?>" class="img-left" >
                            <?php endif; ?>
                        </div>
                        <div class="info-lobo left-info">
                            <h1><b><?php the_field("lobo_titulo") ?></b></h1>
                            <h4>Idade: <?php the_field("lobo_idade") ?> anos</h4>
                            <p><b><?php the_field("lobo_descr") ?></b></p>
                        </div>
                    </div>
                    <?php } 
                    else{ ?>
                    <div class="bloco-lobo right">
                        <div class="info-lobo-right right-info">
                            <h1><b><?php the_field("lobo_titulo") ?></b></h1>
                            <h4>Idade: <?php the_field("lobo_idade") ?> anos</h4>
                            <p><b><?php the_field("lobo_descr") ?></b></p>
                        </div>
                        <div class="img-lobo">
                            
                            <?php if( get_field('lobo_foto') ): ?>
                                <img src="<?php the_field('lobo_foto'); ?>" class="img-right" >
                            <?php endif; ?>
                        
                        </div>
                    </div>
                    <?php }
                    $numero++; ?>
                    <?php
                endwhile;
                wp_reset_postdata();   
                ?>    
    </main>
<?php get_footer(); ?>